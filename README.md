# common-knowledge
公共知识库

## git

- [官网中文文档](https://git-scm.com/book/zh/v2)
- [菜鸟教程](http://www.runoob.com/git/git-tutorial.html)
- [廖雪峰git教程](https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)


## markdown

- [Markdown 语法说明 (简体中文版)](https://www.appinn.com/markdown/)
- [知乎：怎样引导新手使用 Markdown？](https://www.zhihu.com/question/20409634)


## 更新日志 
### 20180127
各组组长尽快创建所需项目，以便规划文档、设计稿和代码的版本管理。

readme中写清楚项目的基本信息，主要功能、当前规划、所使用的开源项目及链接、相关连项目链接等。
wiki中对项目情况和相关知识进一步更详细的记录（对基本概念解释时附相关主要参考链接）。

代码中注释量原则上不低于三分之一

设计稿和设计文档根据所属项目模块或编程语言归类，在项目中创建docs文件夹，放置其它格式的文档和设计稿。比如界面设计归前端的项目、数据库设计文档等归后台